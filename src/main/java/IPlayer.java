import javax.swing.*;
import java.awt.*;

public interface IPlayer {

    //Interfejs


    //Lokalizacja obrazów
    public final String pathToStandardShipTypeImg = "src/main/resources/SpaceShip.png";
    public final String pathToArcticShipTypeImg = "src/main/resources/SpaceShipArctic.png";
    public final String pathToNeoShipTypeImg = "src/main/resources/SpaceShipNeo.png";
    public final String pathToStandardBulletTypeImg = "src/main/resources/BulletOrg.png";
    public final String pathToExtraDmgBulletTypeImg = "src/main/resources/BulletRed.png";
    public final String pathToLessDmgBulletTypeImg = "src/main/resources/BulletBlue.png";


    Rectangle getBounds();
    int getDistance();
    int getDamage();
    int getX();
    int getY();
    void setDamage(int d);
    void setDistance(int d);
    void setX(int x);
    void setY(int y);
    void moveLeft();
    void moveRight();
    void stopMoving();
    void fire();
    Image getImage();
    void move();
    int getWidth();
    int getHeight();
    void setHealth(int h);
    int getHealth();
    java.util.List<Bullet>  getPlayerBullets();
    void addNumOfBullets(int n);
    int getNumOfBullets();
    void setBulletTypeImg(ImageIcon img);
    void setSpaceShipImage(String source);

}
